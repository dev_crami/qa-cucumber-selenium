@runThis
Feature: Log-in the application

  Scenario: Correct Login
    Given User is at login home page 1
    And A valid user and password
    When User types correct user and password
    And Clicks Login
    Then User navigates to his profile page

  Scenario Outline: Incorrect Login
    Given User is at login home page 2
    When User type <user> and <password>
    Then Clicks login button
    And <message> is shown
    And Message is orange

    Examples:
    |user     |password     |message                  |
    |         |             |Username cannot be empty |
    |Admin    |             |Password cannot be empty |
    |         |admin123     |Username cannot be empty |
    |wronguser|wrongpass    |Invalid credentials      |

  Scenario: Login using Tab key to navigate
    Given User is at login home page 3
    And Focus is in Username input
    When User types username
    And Press Tab key
    And User types password
    And Press Enter key
    Then User navigates to his profile page