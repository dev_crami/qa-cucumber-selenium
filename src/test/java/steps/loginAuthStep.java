package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.junit.Assert.assertEquals;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import runner.CucumberRunner;

public class loginAuthStep {

    private WebDriver driver;
    private String browser_type;        // "chrome" | "firefox" | "ie"

    private WebElement we_username;
    private WebElement we_password;
    private WebElement we_btnlogin;

    private String validuser;
    private String validpassword;

    CucumberRunner runner = new CucumberRunner();

    // Method that controls if Browsers Interface are shown or not.
    // Just bu changing bool to: True / False
    private boolean loadHeadless(){
        return true;
    }

    @Given("User is at login home page 1")
    public void user_is_at_login_home_page_1() {
        browser_type = runner.getBrowser();

        if (browser_type.compareTo("chrome") == 0){
            System.out.println("--- CHROME Test ---");
            System.setProperty("webdriver.chrome.driver", "./src/test/resources/drivers/chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.setHeadless(loadHeadless());
            driver = new ChromeDriver(options);
        } else if (browser_type.compareTo("firefox") == 0){
            System.out.println("--- FIREFOX Test ---");
            System.setProperty("webdriver.firefox.bin", System.getenv("USERPROFILE")+"\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
            System.setProperty("webdriver.gecko.driver", "./src/test/resources/drivers/geckodriver.exe");
            FirefoxOptions options = new FirefoxOptions();
            options.setHeadless(loadHeadless());
            driver = new FirefoxDriver(options);
        }
        System.out.println("User is at the Home Page");
        driver.manage().window().maximize();
        driver.get("https://opensource-demo.orangehrmlive.com/");
    }

    @Given("User is at login home page 2")
    public void user_is_at_login_home_page_2() {
        browser_type = runner.getBrowser();

        if (browser_type.compareTo("chrome") == 0){
            System.out.println("--- CHROME Test ---");
            System.setProperty("webdriver.chrome.driver", "./src/test/resources/drivers/chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.setHeadless(loadHeadless());
            driver = new ChromeDriver(options);
        } else if (browser_type.compareTo("firefox") == 0){
            System.out.println("--- FIREFOX Test ---");
            System.setProperty("webdriver.firefox.bin", System.getenv("USERPROFILE")+"\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
            System.setProperty("webdriver.gecko.driver", "./src/test/resources/drivers/geckodriver.exe");
            FirefoxOptions options = new FirefoxOptions();
            options.setHeadless(loadHeadless());
            driver = new FirefoxDriver(options);
        }
        System.out.println("User is at the Home Page");
        driver.manage().window().maximize();
        driver.get("https://opensource-demo.orangehrmlive.com/");
    }

    @Given("User is at login home page 3")
    public void user_is_at_login_home_page_3() {
        browser_type = runner.getBrowser();

        if (browser_type.compareTo("chrome") == 0){
            System.out.println("--- CHROME Test ---");
            System.setProperty("webdriver.chrome.driver", "./src/test/resources/drivers/chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.setHeadless(loadHeadless());
            driver = new ChromeDriver(options);
        } else if (browser_type.compareTo("firefox") == 0){
            System.out.println("--- FIREFOX Test ---");
            System.setProperty("webdriver.firefox.bin", System.getenv("USERPROFILE")+"\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
            System.setProperty("webdriver.gecko.driver", "./src/test/resources/drivers/geckodriver.exe");
            FirefoxOptions options = new FirefoxOptions();
            options.setHeadless(loadHeadless());
            driver = new FirefoxDriver(options);
        }
        System.out.println("User is at the Home Page");
        driver.manage().window().maximize();
        driver.get("https://opensource-demo.orangehrmlive.com/");
    }

    @When("User types correct user and password")
    public void user_types_correct_user_and_password() {
        System.out.println("User is completing fields");
        we_username = driver.findElement((By.id("txtUsername")));
        we_username.clear();
        we_username.sendKeys(validuser);
        we_password = driver.findElement((By.id("txtPassword")));
        we_password.clear();
        we_password.sendKeys(validpassword);
    }

    @When("^User type (.*) and (.*)")
    public void user_types_and(String user, String password) {
        System.out.println("User is completing fields");
        we_username = driver.findElement((By.id("txtUsername")));
        we_username.clear();
        we_username.sendKeys(user);
        we_password = driver.findElement((By.id("txtPassword")));
        we_password.clear();
        we_password.sendKeys(password);
    }

    @When("User types username")
    public void user_types_username() {
        System.out.println("User is completing fields");
        we_username = driver.findElement((By.id("txtUsername")));
        we_username.clear();
        we_username.sendKeys("Admin");
    }

    @Then("User navigates to his profile page")
    public void user_navigates_to_his_profile_page() throws InterruptedException {
        Thread.sleep(2000);

        assertEquals("https://opensource-demo.orangehrmlive.com/index.php/dashboard",driver.getCurrentUrl());
        System.out.println("User is at his Profile syte");

        driver.close();
    }

    @Then("Clicks login button")
    public void clicks_login_button() {
        System.out.println("User clicks login button");
        we_btnlogin = driver.findElement((By.id("btnLogin")));
        we_btnlogin.click();
    }

    @And("A valid user and password")
    public void a_valid_user_and_password_chrome() {
        System.out.println("User is completing fields");
        we_username = driver.findElement((By.id("txtUsername")));
        validuser = "Admin";
        we_password = driver.findElement((By.id("txtPassword")));
        validpassword = "admin123";
    }

    @And("Clicks Login")
    public void clicks_login() {
        System.out.println("User clicks login button");
        we_btnlogin = driver.findElement((By.id("btnLogin")));
        we_btnlogin.click();
    }

    @And("^(.*) is shown$")
    public void is_shown(String message) {
        System.out.println("Checking message");
        assertEquals(message,driver.findElement((By.id("spanMessage"))).getText());
    }

    @And("Message is orange")
    public void message_is_orange() {
        System.out.println("Checking message color");
        if (browser_type.compareTo("chrome") == 0){
            System.out.println("Chrome color RGBA");
            assertEquals("rgba(221, 119, 0, 1)",driver.findElement((By.id("spanMessage"))).getCssValue("color"));
        }
        if (browser_type.compareTo("firefox") == 0){
            System.out.println("Chrome color RGB");
            assertEquals("rgb(221, 119, 0)",driver.findElement((By.id("spanMessage"))).getCssValue("color"));
        }
        driver.close();
    }

    @And("Focus is in Username input")
    public void focus_is_in_username_input() {
        we_username = driver.findElement((By.id("txtUsername")));
        we_username.click();
    }

    @And("Press Tab key")
    public void press_tab_key() {
        we_username = driver.findElement((By.id("txtUsername")));
        we_username.click();
        we_username.sendKeys(Keys.TAB);
    }

    @And("User types password")
    public void user_types_password() {
        we_password = driver.findElement((By.id("txtPassword")));
        we_password.sendKeys("admin123");
    }

    @And("Press Enter key")
    public void press_enter_key() {
        we_password.sendKeys(Keys.ENTER);
    }
}