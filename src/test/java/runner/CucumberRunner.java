package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * TODO:
 * - Asserts revision.
 * - Paralel exec with TestNG (?)
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        //tags = "@chrome",
        features = "src/test/java/features",
        //glue = "cucumb.features",
        glue = "steps",
        plugin = {"pretty",
                "html:target/cucumber-reports",
                "json:target/cucumber.json"}
)

public class CucumberRunner {

    // You can control the Browser to Execute from Runner, changing "browser" var.

    String browser = "firefox";

    public String getBrowser(){

        return browser;
    }
}
